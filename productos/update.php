<?php require_once('../header.php'); ?>
<style>

</style>
<div class="container">
    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">
            <form method="post" action="">
                <table class="table table-bordered table-responsive table-hover">

<?php
require_once('../conexiones.php');

$id=$_GET['id'];

$sth = $pdo->prepare("SELECT id, descripcion,stock_minimo,stock_maximo from productos WHERE id = :id");
$sth->bindValue(':id', $id, PDO::PARAM_STR); // No select e no delete basta um bindValue
$sth->execute();

$reg = $sth->fetch(PDO::FETCH_OBJ);

$id = $reg->id;
$descripcion = $reg->descripcion;
$stock_minimo = $reg->stock_minimo;
$stock_maximo = $reg->stock_maximo;
?>
   <h3>productos</h3>
   <tr><td>Descripcion</td><td><input name="descripcion" type="text" value="<?=$descripcion?>"></td></tr>
   <tr><td>stock mínimo</td><td><input name="stock_minimo" type="text" value="<?=$stock_minimo?>"></td></tr>
   <tr><td>stock máximo</td><td><input name="stock_maximo" type="text" value="<?=$stock_maximo?>"></td></tr>
   <tr><td></td><td><input name="enviar" class="btn btn-primary" type="submit" value="Editar">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
   <input name="id" type="hidden" value="<?=$id?>">
   <input name="enviar" class="btn btn-warning" type="button" onclick="location='index.php'" value="Regresar"></td></tr>
       </table>
        </form>
        </div>
    <div>
</div>
<?php

if(isset($_POST['enviar'])){
    $id = $_POST['id'];
    $descripcion = $_POST['descripcion'];
    $stock_minimo = $_POST['stock_minimo'];
    $stock_maximo = $_POST['stock_maximo'];

    $data = [
        'descripcion' => $descripcion,
        'stock_minimo' => $stock_minimo,
        'stock_maximo' => $stock_maximo,
        'id' => $id,
    ];

    $sql = "UPDATE productos SET descripcion=:descripcion, stock_minimo=:stock_minimo,stock_maximo=:stock_maximo  WHERE id=:id";
    $stmt= $pdo->prepare($sql);

   if($stmt->execute($data)){
        print "<script>alert('Registro alterado com sucesso!');location='index.php';</script>";
    }else{
        print "Erro ao editar o registro!<br><br>";
    }
}
require_once('../footer.php');
?>
