<!DOCTYPE html>
<html lang="pt-br">
<head>
    <title> floven.com</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@3.4.1/dist/css/bootstrap.min.css" integrity="sha384-HSMxcRTRxnN+Bdg0JdbxYKrThecOKuH5zCYotlSAcp1+c8xmyTe9GYg1l9a69psu" crossorigin="anonymous">
    <style type="text/css">
    * {
  margin: 0;
  padding: 0;
  box-sizing: border-box;
}

/* Estilos generales de la página */
body {
  font-family: Arial, sans-serif;
  background-color: #f1f1f1;
  color: #333333;
}

.container {
  max-width: 960px;
  margin: 0 auto;
  padding: 20px;
}

h1 {
  font-size: 24px;
  font-weight: bold;
  margin-bottom: 20px;
}

p {
  font-size: 16px;
  line-height: 1.5;
  margin-bottom: 10px;
}

a {
  color: #0066cc;
  text-decoration: none;
}

a:hover {
  text-decoration: underline;
}

    </style>
</head>

<body>
<br/>
<div class="container">
    <div class="panel panel-default">
        <div class="jumbotron text-center"><h1><strong>flowen.com</strong></h1></div>