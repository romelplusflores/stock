<?php
$hostname = "localhost";
$username = "root";
$password = "admin";
$database = "stock";
$row_limit = 8;
$sgbd = 'mysql'; // mysql, pgsql

// conexion to mysql
try {
    $pdo = new PDO($sgbd.":host=$hostname;port=3307;dbname=$database", $username, $password);
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    echo "conexion establecida parece que sabes programar";
} catch (PDOException $err) {
    echo "fallo de conexion";
    die("Error! " . $err->getMessage());
}
